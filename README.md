# Tibkcots

but it `reverse()`

## Challenge 01
For anagram challenge can be seeing on file `challenge-01.js`

## React Challenge

For React Challenge demo can be seeing on page [this demo page](https://tibkcots.vercel.app)

### Installation for React Challenge

Minimum requirement for running project application is.

- Node 14.x LTS
- Yarn Packages (Recommended)
- Ubuntu 20.04 / Other Linux Distribution OS (Recommended)

To install the package simply run `yarn` on root projects, and run `yarn start` for starting development, the server will be started at http://localhost:3000.

If you wish to build the application, please run `yarn build` to build the static files and run `yarn start` to start production server.

### Test

This project using `jest` for testing the react component tests. Simply run `yarn test` to run all unit tests.

### Lint and Formatting

This project use `eslint` for linting and `prettier` for code formatting. Simply run `yarn lint` to check code errors and warning. And run `yarn format` to format all codes
