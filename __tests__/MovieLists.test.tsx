import MovieLists from 'components/MovieLists'
import { mount } from 'enzyme'
import React from 'react'
import { Provider } from 'react-redux'
import store from 'store'
import { mockRoute } from './util'

describe('MovieLists', () => {
  it('should render without throwing an error', async () => {
    mockRoute({
      route: '/',
      query: { q: 'dream theater' },
      asPath: '',
      pathname: '/'
    })
    const wrap = mount(
      <Provider store={store}>
        <MovieLists />
      </Provider>
    )
    expect(wrap.find('.movie-lists').length).toBe(1)
  })
})
