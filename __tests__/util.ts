/* eslint-disable @typescript-eslint/no-var-requires */
import MockAdapter from 'axios-mock-adapter'
import { act } from 'react-dom/test-utils'
import Api from 'util/api'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const useRouter = jest.spyOn(require('next/router'), 'useRouter')

export const wait = (ms) =>
  act(() => new Promise((resolve) => setTimeout(resolve, ms)))

export const mockRoute = ({ route, pathname, query, asPath }) => {
  useRouter.mockImplementation(() => ({
    route,
    pathname,
    query,
    asPath,
    prefetch: () => Promise.resolve(true),
    push: () => jest.fn()
  }))
}

export const mockAxios = () => {
  const mock = new MockAdapter(Api)

  const movies = require('__fixtures__/movies.json')
  const movies_p2 = require('__fixtures__/movies-page-2.json')
  mock
    .onAny('/', { params: { s: 'dream theater', page: 1, apikey: undefined } })
    .reply(200, movies)
  mock
    .onGet('/', { params: { s: 'dream theater', page: 2, apikey: undefined } })
    .reply(200, movies_p2)
}
