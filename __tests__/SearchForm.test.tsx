import SearchForm from 'components/SearchForm'
import { mount } from 'enzyme'
import React from 'react'
import { Provider } from 'react-redux'
import store from 'store'
import { mockRoute } from './util'

describe('SearchForm', () => {
  it('should render without throwing an error', async () => {
    mockRoute({ route: '/', query: '', asPath: '', pathname: '/' })
    const wrap = mount(
      <Provider store={store}>
        <SearchForm />
      </Provider>
    )
    expect(wrap.find('.searchForm').length).toBe(1)
  })

  it('should change value if query present', async () => {
    mockRoute({
      route: '/',
      query: { q: 'dream theater' },
      asPath: '',
      pathname: '/'
    })
    const wrap = mount(
      <Provider store={store}>
        <SearchForm />
      </Provider>
    )
    expect(wrap.find('.searchForm input.input').prop('value')).toBe(
      'dream theater'
    )
  })
})
