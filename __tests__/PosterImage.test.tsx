import PosterImage from 'components/PosterImage'
import { mount } from 'enzyme'
import React from 'react'

describe('SearchForm', () => {
  it('should render without throwing an error', async () => {
    const wrap = mount(
      <PosterImage item={{ Poster: 'some-picture.jpg', Title: 'Some title' }} />
    )
    expect(wrap.find('.posterContainer').length).toBe(1)
    expect(wrap.find('img.poster').getDOMNode().getAttribute('alt')).toBe(
      'Some title'
    )
    expect(wrap.find('img.poster').getDOMNode().getAttribute('src')).toBe(
      'some-picture.jpg'
    )
    wrap.unmount()
  })

  it('should open modal', async () => {
    jest.useFakeTimers()

    const wrap = mount(
      <PosterImage item={{ Poster: 'some-picture.jpg', Title: 'Some title' }} />
    )
    wrap.find('.poster').simulate('click')

    jest.runTimersToTime(300)

    expect(
      document.querySelector('.modal-dialog .modal-title').textContent
    ).toBe('Some title')

    expect(
      document.querySelector('.modal-dialog .modalImage').getAttribute('alt')
    ).toBe('Some title')

    expect(
      document.querySelector('.modal-dialog .modalImage').getAttribute('src')
    ).toBe('some-picture.jpg')
  })
})
