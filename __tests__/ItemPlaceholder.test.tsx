import ItemPlaceholder from 'components/ItemPlaceholder'
import { mount } from 'enzyme'
import React from 'react'

describe('DetailPlaceholder', () => {
  it('should render without throwing an error', async () => {
    const wrap = mount(<ItemPlaceholder />)
    expect(wrap.find('.placeholder').length).toBe(1)
  })
})
