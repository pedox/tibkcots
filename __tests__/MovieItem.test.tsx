/* eslint-disable @typescript-eslint/no-var-requires */
import MovieItem from 'components/MovieItem'
import { mount } from 'enzyme'
import React from 'react'

describe('MovieItem', () => {
  it('should render without throwing an error', async () => {
    const movie = require('__fixtures__/movie.json')

    const wrap = mount(<MovieItem item={movie} />)
    expect(wrap.find('.movieItem').length).toBe(1)
    expect(wrap.find('.title').text()).toBe('Dream Theater: Live at Budokan')
    expect(wrap.find('.year').text()).toBe('2004')
    expect(wrap.find('.type').text()).toBe('movie')
    expect(wrap.find('.posterContainer').length).toBe(1)
  })
})
