import { mount } from 'enzyme'
import React from 'react'
import Base from 'components/Base'

describe('BaseTemplate', () => {
  it('should render without throwing an error', async () => {
    const wrap = mount(
      <Base>
        <div className="main-content">This is a children</div>
      </Base>
    )
    expect(wrap.find('.main-content').length).toBe(1)
  })
})
