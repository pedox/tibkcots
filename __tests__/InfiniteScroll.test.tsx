/* eslint-disable @typescript-eslint/no-var-requires */
import InfiniteScroll from 'components/InfiniteScroll'
import MovieLists from 'components/MovieLists'
import SearchForm from 'components/SearchForm'
import { mount, ReactWrapper } from 'enzyme'
import React from 'react'
import { Provider } from 'react-redux'
import store from 'store'
import { mockAxios, mockRoute, wait } from './util'

const events = {} as any

document.addEventListener = jest.fn((event, cb) => {
  events[event] = cb
})

mockAxios()

describe('InfiniteScroll', () => {
  let wrap: ReactWrapper

  beforeEach(() => {
    mockRoute({
      route: '/',
      query: '',
      asPath: '',
      pathname: '/'
    })
    wrap = mount(
      <Provider store={store}>
        <SearchForm />
        <InfiniteScroll>
          <MovieLists />
        </InfiniteScroll>
      </Provider>
    )
  })

  afterEach(() => {
    wrap.unmount()
  })

  it('should render without throwing an error', () => {
    expect(wrap.find('.infinite-scroll').length).toBe(1)
  })

  it('should click load more', async () => {
    wrap.find('.searchForm input.input').simulate('change', {
      persist: () => {
        //..
      },
      target: { name: 'query', value: 'dream theater' }
    })
    wrap.find('.searchForm form').simulate('submit')

    await wait(100)
    wrap.update()

    expect(wrap.find('.movie-lists .movieItem').length).toBe(5)
    events.scroll()

    await wait(100)
    wrap.update()

    expect(wrap.find('.movie-lists .movieItem').length).toBe(10)

    events.scroll()

    await wait(100)
    wrap.update()

    expect(wrap.find('.movie-lists .movieItem').length).toBe(12)

    // expect()).toBe(1)
  })
})
