module.exports = {
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  preset: 'ts-jest',
  testRegex: ['(/__tests__/.*(\\.|/)(test))\\.tsx?$'],
  setupFilesAfterEnv: ['<rootDir>/enzyme.js'],
  moduleNameMapper: {
    '\\.(svg)$': '<rootDir>/file-mocks.js',
    '\\.(scss)$': 'identity-obj-proxy'
  },
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.jest.json'
    }
  },
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  moduleDirectories: ['node_modules', '.']
}
