import axios from 'axios'

const Api = axios.create({
  baseURL: `//www.omdbapi.com`,
  params: { apikey: process.env.NEXT_PUBLIC_OMDBAPIKEY }
})

export default Api
