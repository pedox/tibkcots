/* eslint-disable */
var test = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']

function generateAnagram(words) {
  var anagram = []

  function checkSort(words) {
    var chunks = words.split('')
    var i = 1
    while (i < chunks.length) {
      var j = 0
      while (j < i) {
        if (chunks[i] < chunks[j]) {
          tmp = chunks[i]
          chunks[i] = chunks[j]
          chunks[j] = tmp
        }
        j++
      }
      i++
    }
    return chunks.join('')
  }

  var i = 0
  while (words[i]) {
    var j = 0
    var found = false
    while (anagram[j]) {
      if (checkSort(anagram[j][0]) === checkSort(words[i])) {
        anagram[j].push(words[i])
        found = true
      }
      j++
    }
    if (found === false) {
      anagram.push([words[i]])
      words.splice(i, 1)
      continue
    }
    i++
  }

  return anagram
}

console.log(generateAnagram(test))
