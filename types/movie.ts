export type MovieType = 'movie' | 'series' | 'episode'

export interface Movie {
  Title: string
  Year: string
  imdbID: string
  Type: MovieType
  Poster: string
}

export interface MovieSearchResponse {
  Search: Movie[]
  totalResults: string
  Response: string
  Error?: string
}

export interface MovieRating {
  Source: string
  Value: string
}

export interface MovieDetail {
  Actors: string
  Awards: string
  BoxOffice: string
  Country: string
  DVD: string
  Director: string
  Genre: string
  Language: string
  Metascore: string
  Plot: string
  Poster: string
  Production: string
  Rated: string
  Ratings: MovieRating[]
  Released: string
  Runtime: string
  Title: string
  Type: string
  Website: string
  Writer: string
  Year: string
  imdbID: string
  imdbRating: string
  imdbVotes: string
}

export interface MovieDetailResponse extends MovieDetail {
  Response: string
  Error?: string
}
