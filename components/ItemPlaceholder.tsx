import React from 'react'

import style from './ItemPlaceholder.module.scss'

const ItemPlaceholder = () => {
  return (
    <div className={style.placeholder}>
      <div className={style.cover}></div>
      <div className={style.main}>
        <div className={style.title}></div>
        <div className={style.year}></div>
        <div className={style.type}></div>
      </div>
    </div>
  )
}

export default ItemPlaceholder
