import { Form, Formik } from 'formik'
import { Input, Submit } from 'formstrap'
import { useRouter } from 'next/dist/client/router'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'store'
import { movieSearch } from 'store/slices/moviesSlice'
import style from './SearchForm.module.scss'

const SearchForm = () => {
  const { query, push } = useRouter()
  const { search } = useSelector((state: RootState) => state.movieSlice)
  const dispatch = useDispatch()

  const onSubmit = async (values) => {
    push(`/?q=${values.query}`)
    await dispatch(movieSearch({ ...values, page: 1 }))
  }

  useEffect(() => {
    if (query.q && search.query !== query.q) {
      dispatch(movieSearch({ query: query.q as string, page: 1 }))
    }
  }, [query])

  return (
    <div className={style.searchForm}>
      <Formik enableReinitialize initialValues={search} onSubmit={onSubmit}>
        <Form className={style.form}>
          <Input
            type="text"
            name="query"
            placeholder="Search"
            className={style.input}
          />
          <Submit>Search</Submit>
        </Form>
      </Formik>
    </div>
  )
}

export default SearchForm
