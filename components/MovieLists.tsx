import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from 'store'
import MovieItem from './MovieItem'

const MovieLists = () => {
  const { data, currentCursor } = useSelector(
    (state: RootState) => state.movieSlice
  )

  return (
    <div className="movie-lists">
      {data.data
        .filter((_, index) => index < currentCursor)
        .map((item, index) => (
          <MovieItem item={item} key={`${item.imdbID}-${index}`} />
        ))}
    </div>
  )
}

export default MovieLists
