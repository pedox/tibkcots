import Link from 'next/link'
import React from 'react'
import style from './Base.module.scss'

interface BaseProp {
  children?: React.ReactNode
}

const Base = ({ children }: BaseProp) => {
  return (
    <div className={style.base}>
      <div className={style.header}>
        <Link href="/">
          <a>
            <h1 className="text-dark">MovieDB</h1>
          </a>
        </Link>
      </div>
      <section className={style.main}>{children}</section>
    </div>
  )
}

export default Base
