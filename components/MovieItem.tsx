import React from 'react'
import { Movie } from 'types/movie'
import style from './MovieItem.module.scss'
import Link from 'next/link'
import PosterImage from './PosterImage'
interface MovieItemProp {
  item: Movie
}

const MovieItem = ({ item }: MovieItemProp) => {
  return (
    <div className={style.movieItem}>
      <div>
        <PosterImage item={item} />
      </div>
      <div className={style.mainInfo}>
        <Link href={`/item/${item.imdbID}`}>
          <a>
            <h1 className={style.title}>{item.Title}</h1>
          </a>
        </Link>
        <p className={style.year}>{item.Year}</p>
        <p className={style.type}>{item.Type}</p>
      </div>
    </div>
  )
}

export default MovieItem
