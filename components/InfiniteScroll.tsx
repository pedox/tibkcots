import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'store'
import { loadMoreMovie } from 'store/slices/moviesSlice'

interface InfiniteScrollProp {
  children: React.ReactNode
}

const InfiniteScroll = ({ children }: InfiniteScrollProp) => {
  const isLoading = useSelector(
    (state: RootState) => state.movieSlice.data.isLoading
  )

  const currentCursor = useSelector(
    (state: RootState) => state.movieSlice.currentCursor
  )

  const dispatch = useDispatch()
  const containerElm = useRef<HTMLDivElement>(null)
  const isDoingLoadMore = useRef<boolean>(false)

  const onLoadMore = () => {
    dispatch(loadMoreMovie())
  }

  const onWindowScroll = () => {
    if (containerElm.current) {
      const heightFromTop =
        containerElm.current.offsetTop + containerElm.current.clientHeight

      if (window.scrollY + window.outerHeight > heightFromTop) {
        if (isDoingLoadMore.current === false) {
          onLoadMore()
          isDoingLoadMore.current = true
        }
      }
    }
  }

  useEffect(() => {
    setTimeout(() => {
      isDoingLoadMore.current = isLoading
    }, 100)
  }, [isLoading])

  useEffect(() => {
    setTimeout(() => {
      isDoingLoadMore.current = false
    }, 100)
  }, [currentCursor])

  useEffect(() => {
    document.addEventListener('scroll', onWindowScroll)
    return () => {
      document.removeEventListener('scroll', onWindowScroll)
    }
  }, [])

  return (
    <div className="infinite-scroll" ref={containerElm}>
      {children}
    </div>
  )
}

export default InfiniteScroll
