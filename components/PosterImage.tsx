import React, { useState } from 'react'
import { Modal, ModalBody, ModalHeader } from 'reactstrap'
import style from './PosterImage.module.scss'

interface Props {
  item: {
    Title: string
    Poster: string
  }
}

const PosterImage = ({ item }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false)

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen)
  }

  return (
    <figure className={style.posterContainer}>
      {item.Poster === 'N/A' ? (
        <div className={style.noImage} />
      ) : (
        <img
          src={item.Poster}
          className={style.poster}
          alt={item.Title}
          onClick={toggleModal}
          aria-hidden="true"
        />
      )}
      <Modal centered isOpen={isModalOpen} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>{item.Title}</ModalHeader>
        <ModalBody>
          <img
            src={item.Poster}
            className={style.modalImage}
            alt={item.Title}
          />
        </ModalBody>
      </Modal>
    </figure>
  )
}

export default PosterImage
