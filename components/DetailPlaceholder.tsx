import clsx from 'clsx'
import React from 'react'
import { Col, Row } from 'reactstrap'

import style from './DetailPlaceholder.module.scss'

const DetailPlaceholder = () => {
  return (
    <div className={style.placeholder}>
      <div>
        <div className={style.cover}></div>
      </div>
      <div className={style.main}>
        <div className={clsx(style.placeholderLine, style.line1)}></div>
        <div className={clsx(style.placeholderLine, style.line2)}></div>
        <div className={clsx(style.placeholderLine, style.line3)}></div>
        <Row className="mt-4">
          {[...new Array(8)].map((_, index) => (
            <Col md={6} key={index} className="mb-2">
              <div className={clsx(style.placeholderLine, style.line2)} />
              <div className={clsx(style.placeholderLine, style.line3)} />
            </Col>
          ))}
        </Row>
        <Row className="mt-4">
          {[...new Array(2)].map((_, index) => (
            <Col md={12} key={index} className="mb-2">
              <div className={clsx(style.placeholderLine, style.line2)} />
              <div className={clsx(style.placeholderLine, style.line1)} />
            </Col>
          ))}
        </Row>
        <Row className="mt-4">
          {[...new Array(2)].map((_, index) => (
            <Col md={6} key={index} className="mb-2">
              <div className={clsx(style.placeholderLine, style.line2)} />
              <div className={clsx(style.placeholderLine, style.line3)} />
            </Col>
          ))}
        </Row>
      </div>
    </div>
  )
}

export default DetailPlaceholder
