import { configureStore } from '@reduxjs/toolkit'
import movieDetailSlice from './slices/movieDetailSlice'
import movieSlice from './slices/moviesSlice'

const store = configureStore({
  reducer: {
    movieSlice,
    movieDetailSlice
  }
})

export type RootState = ReturnType<typeof store.getState>

export default store
