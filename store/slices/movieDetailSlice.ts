import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { MovieDetailResponse } from 'types/movie'
import Api from 'util/api'

export const loadMovieDetail = createAsyncThunk<
  MovieDetailResponse,
  { id: string }
>('movie-detail/fetch', async ({ id }) => {
  const { data } = await Api.get('/', {
    params: {
      i: id,
      plot: 'full'
    }
  })
  return data
})

export const movieDetailSlice = createSlice({
  name: 'movie-detail',
  initialState: {
    id: null,
    isLoading: true,
    error: null,
    data: null as MovieDetailResponse
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loadMovieDetail.pending, (state, { meta }) => {
      state.isLoading = true
      state.id = meta.arg.id
    })
    builder.addCase(loadMovieDetail.fulfilled, (state, { payload }) => {
      state.isLoading = false
      if (payload.Error) {
        state.error = payload.Error
        state.data = null
      } else {
        state.error = null
        state.data = typeof payload !== 'string' ? payload : null
      }
    })
    builder.addCase(loadMovieDetail.rejected, (state, { error }) => {
      state.isLoading = false
      state.error = error.message || `Connection error`
    })
  }
})

export default movieDetailSlice.reducer
