import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { RootState } from 'store'
import { Movie, MovieSearchResponse } from 'types/movie'
import Api from 'util/api'

const MAX_CURSOR = 5

export const loadMoreMovie = createAsyncThunk(
  'movie/load-more',
  (_body, { dispatch, getState }) => {
    const state = getState() as RootState
    const curentCursor = state.movieSlice.currentCursor
    const nextCursor = curentCursor + MAX_CURSOR
    const total = state.movieSlice.data.total
    if (nextCursor < total || (total > curentCursor && total < nextCursor)) {
      dispatch(movieSlice.actions.setNextCursor())
      if (nextCursor > state.movieSlice.data.data.length) {
        return dispatch(
          movieSearch({
            ...state.movieSlice.search,
            page: state.movieSlice.search.page + 1
          })
        )
      }
    }
  }
)

export const movieSearch = createAsyncThunk<
  MovieSearchResponse,
  { query: string; page: number }
>('movie/search', async (body) => {
  const { data } = await Api.get('/', {
    params: {
      s: body.query,
      page: body.page || 1
    }
  })
  return data
})

const movieSlice = createSlice({
  name: 'movie',
  initialState: {
    currentCursor: 5,
    search: {
      query: '',
      page: 1
    },
    data: {
      isLoading: false,
      error: null,
      data: [] as Movie[],
      currentPage: 1,
      total: 0
    }
  },
  reducers: {
    setNextCursor: (state) => {
      state.currentCursor += MAX_CURSOR
    }
  },
  extraReducers: (builder) => {
    builder.addCase(movieSearch.pending, (state, { meta }) => {
      state.search = meta.arg
      state.data.isLoading = true
      state.data.currentPage = meta.arg.page
      if (state.data.currentPage === 1) {
        state.currentCursor = MAX_CURSOR
      }
    })
    builder.addCase(movieSearch.fulfilled, (state, { payload }) => {
      state.data.isLoading = false
      if (payload.Error) {
        state.data.error = payload.Error
        state.data.data = []
        state.data.total = 0
      } else {
        state.data.error = null
        state.data.total = parseInt(payload.totalResults)
        if (state.search.page > 1) {
          state.data.data = [...state.data.data, ...payload.Search]
        } else {
          state.data.data = payload.Search
        }
      }
    })
    builder.addCase(movieSearch.rejected, (state, { error }) => {
      state.data.isLoading = false
      state.data.error = error.message || `Connection error`
    })
  }
})

export default movieSlice.reducer
