import InfiniteScroll from 'components/InfiniteScroll'
import ItemPlaceholder from 'components/ItemPlaceholder'
import React from 'react'
import { useSelector } from 'react-redux'
import Base from 'components/Base'
import MovieItem from 'components/MovieItem'
import SearchForm from 'components/SearchForm'
import { RootState } from 'store'
import Head from 'next/head'

const IndexPage = () => {
  const { data, currentCursor } = useSelector(
    (state: RootState) => state.movieSlice
  )

  return (
    <Base>
      <Head>
        <title>MovieDB</title>
      </Head>
      <SearchForm />
      {data.isLoading && data.currentPage === 1 ? (
        [...new Array(5)].map((_, index) => (
          <ItemPlaceholder key={`placeholder-${index}`} />
        ))
      ) : (
        <InfiniteScroll>
          <p>Found {data.total} results</p>
          {data.data
            .filter((_, index) => index < currentCursor)
            .map((item, index) => (
              <MovieItem item={item} key={`${item.imdbID}-${index}`} />
            ))}
        </InfiniteScroll>
      )}
    </Base>
  )
}

export default IndexPage
