import React from 'react'
import { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import store from 'store'

import 'styles/global.scss'

const CustomApp = ({ Component, pageProps }: AppProps) => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default CustomApp
