import Base from 'components/Base'
import DetailPlaceholder from 'components/DetailPlaceholder'
import PosterImage from 'components/PosterImage'
import { useRouter } from 'next/dist/client/router'
import Head from 'next/head'
import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Col, Row } from 'reactstrap'
import { RootState } from 'store'
import { loadMovieDetail } from 'store/slices/movieDetailSlice'
import style from './item.module.scss'

const DetailPage = () => {
  const { query } = useRouter()
  const { data, error, isLoading, id } = useSelector(
    (state: RootState) => state.movieDetailSlice
  )
  const dispatch = useDispatch()

  useEffect(() => {
    if (query?.id) {
      dispatch(loadMovieDetail({ id: query.id as string }))
    }
  }, [query])

  return (
    <Base>
      <Head>
        <title>MovieDB</title>
      </Head>
      {isLoading ? (
        <DetailPlaceholder />
      ) : error ? (
        <div className="error">
          <h2>Something went wrong!</h2>
          <p>{error}</p>
        </div>
      ) : data !== null ? (
        <div className={style.detailPage} key={id}>
          <Head>
            <title>{data.Title} - MovieDB</title>
          </Head>
          <PosterImage item={data} />
          <div className={style.main}>
            <h1 className={style.title}>{data.Title}</h1>
            <p className="mb-0">{data.Year}</p>
            <p>{data.Type}</p>
            <div className="mb-4">
              <dt>Plot</dt>
              <dd>{data.Plot}</dd>
            </div>
            <Row>
              <Col md={6}>
                <dt>Director</dt>
                <dd>{data.Director}</dd>
              </Col>
              <Col md={6}>
                <dt>Released</dt>
                <dd>{data.Released}</dd>
              </Col>
              <Col md={6}>
                <dt>Genre</dt>
                <dd>{data.Genre}</dd>
              </Col>
              <Col md={6}>
                <dt>Rated</dt>
                <dd>{data.Rated}</dd>
              </Col>
              <Col md={6}>
                <dt>Production</dt>
                <dd>{data.Production}</dd>
              </Col>
              <Col md={6}>
                <dt>Language</dt>
                <dd>{data.Language}</dd>
              </Col>
              <Col md={6}>
                <dt>Country</dt>
                <dd>{data.Country}</dd>
              </Col>
              <Col md={6}>
                <dt>Meta Score</dt>
                <dd>{data.Metascore}</dd>
              </Col>

              <Col md={12}>
                <dt>Actors</dt>
                <dd>{data.Actors}</dd>
              </Col>
              <Col md={12}>
                <dt>Writer</dt>
                <dd>{data.Writer}</dd>
              </Col>
              <Col md={12}>
                <dt>Awards</dt>
                <dd>{data.Awards}</dd>
              </Col>

              <Col md={6}>
                <dt>IMDB Rating</dt>
                <dd>{data.imdbRating}</dd>
              </Col>
              <Col md={6}>
                <dt>IMDB Votes</dt>
                <dd>{data.imdbVotes}</dd>
              </Col>
            </Row>

            <div className="mt-4">
              <h2 className={style.subTitle}>Ratings</h2>
              <dl>
                {data.Ratings.map((item, index) => (
                  <Fragment key={index}>
                    <dt>{item.Source}</dt>
                    <dd>{item.Value}</dd>
                  </Fragment>
                ))}
              </dl>
            </div>
          </div>
        </div>
      ) : (
        <div className="not-found">
          <h2>Not Found</h2>
          <p>IMDB Data Not Found</p>
        </div>
      )}
    </Base>
  )
}

export default DetailPage
